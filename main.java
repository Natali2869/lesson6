package Task6;

public class main {

    public static void main(String[] args) {

        String input = "1 +2".replace( " ", "");
        if (input.matches("[0-9]*+[-+*/]+[0-9]")) {
            if (input.contains("+")){
                String [] arr = input.split("\\+");
                System.out.println(Double.parseDouble(arr[0]) + Double.parseDouble(arr[1]));
            }
            else if (input.contains("-")){
                String [] arr = input.split( "\\-");
                System.out.println(Double.parseDouble(arr[0]) - Double.parseDouble(arr[1]));
            }
            else if (input.contains("*")){
                String [] arr = input.split("\\*");
                System.out.println(Double.parseDouble(arr[0]) * Double.parseDouble(arr[1]));
            }
            else if (input.contains("/")){
                String [] arr = input.split("\\/");
                if (Double.parseDouble( arr[1]) == 0)
                    System.out.println(0);
                else
                    System.out.println(Double.parseDouble(arr[0]) / Double.parseDouble(arr[1]));
            }
        }
    }
}